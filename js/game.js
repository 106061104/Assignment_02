var mainState = {

    preload: function() {
      

    },

    create: function() {

        
        this.bg = game.add.sprite(0,0,'bg');
        this.bg.scale.setTo(2, 1.5);
       

            
        this.player = game.add.sprite(game.width/2, 280, 'player');
        this.player.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        this.cursor = game.input.keyboard.createCursorKeys();
    },

  
    update: function() {
        this.movePlayer();
    },

  
    movePlayer: function() {
        if(this.cursor.left.isDown){
            this.player.x-=5;
      
                this.player.scale.x = -1 ;
           
        }
        else if(this.cursor.right.isDown){
            this.player.x+=5;
            
                this.player.scale.x = 1 ;
            
        }
        else if(this.cursor.up.isDown){
            this.player.y-=5;
        }
        else if(this.cursor.down.isDown){
            this.player.y+=5;
        }

    }
};



