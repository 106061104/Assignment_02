var entry=1;
var menuState = {
    create: function() {
        // Add a background image
        var bg = game.add.sprite(game.width/2, game.height/2, 'menu_bg');
        bg.anchor.setTo(0.5,0.5);
        game.stage.backgroundColor = '#0';
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 80, 'Super Raiden',
        
        { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen
        /*
        var scoreLabel = game.add.text(game.width/2, game.height/2,
        'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        */
        // Explain how to start the game
        var singleLabel = game.add.text(game.width/2, game.height-300,
        'Single player', { font: '25px Arial', fill: '#ffffff' });
        singleLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
    
        game.add.text(game.width/2-80, game.height-150,
            'press 1 to start', { font: '25px Arial', fill: '#ffffff' });
        

        this.key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.key1.onDown.add(this.start, this);

        key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        key2.onDown.add(this.mulstart, this);

        key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        key3.onDown.add(this.help, this);

        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.ONE);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.TWO);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.THREE);
  //      this.upKey.onDown.add(this.start, this);
        /*
        this.star = game.add.sprite(game.width/2-100, game.height-300, 'star');
        this.star.anchor.setTo(0.5, 0.5);
        this.star.scale.setTo(0.2,0.2);
        */
        this.bgm = game.add.audio('menubgm');
        this.bgm.loopFull();  
    },

    start: function() {
     //Start the actual game
     this.bgm.stop();
    game.state.start('play');
    },
    update: function(){      
            
    },
    mulstart: function(){

    },
    help: function(){

    }
    
    
};