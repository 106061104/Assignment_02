var loadState = {
    preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        
        game.load.image('player', 'assets/player.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('bg', 'assets/bg.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('fireball', 'assets/fireball.png');
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('heart', 'assets/live.png');
        game.load.image('enemy2', 'assets/enemy2.png');
        game.load.image('leaderboard', 'assets/leaderboard.png');
        game.load.image('star', 'assets/star.png');
        game.load.image('helper', 'assets/helper.png');
        game.load.image('shield', 'assets/shield.png');
        game.load.image('autoaim', 'assets/autoaim.png');
        game.load.audio('blaster', 'assets/audio/blaster.mp3');
        game.load.audio('explode', 'assets/audio/explode.mp3');
        game.load.audio('alarm', 'assets/audio/alarm.mp3');
        game.load.audio('bgm', 'assets/audio/running.mp3');
        game.load.audio('menubgm', 'assets/audio/menubgm.mp3');
        game.load.audio('hurt', 'assets/audio/hurt.mp3');
        game.load.audio('bgm_boss', 'assets/audio/bgm_boss.mp3');
        game.load.audio('win', 'assets/audio/win.mp3');
        game.load.image('boom', 'assets/boom.png');
        game.load.image('pillar_L', 'assets/pillar_L.png');
        game.load.image('pillar_R', 'assets/pillar_R.png');
        game.load.image('pillar', 'assets/pillar.png');
        game.load.image('suck_ball', 'assets/suck_ball.png');
        game.load.spritesheet('suck_ball', 'assets/suck_ball.png', 30, 30);
        game.load.image('boss', 'assets/boss.png');
        game.load.image('milos_hurt', 'assets/milos_hurt.png');
        game.load.image('milos_wait', 'assets/milos_wait.png');
        game.load.image('menu_bg', 'assets/menu_bg.png');
        game.load.image('bossbar', 'assets/bossbar.png');

    },
    create: function() {
    // Go to the menu state
       game.state.start('menu');
    }
};