var bulletTime = 0;
var ultTime = 0;
var firingTimer = 0;
var firingTimer2 = 0;
var statetime;

var nowEnemy ;

var ultnum =3;
var ultshow;

var speed =1;


var kills = 0;
var rank = ['none','none','none','none','none','none'];
var score = [0,0,0,0,0,0];

var itemzone =['shield', 'helper', 'recover','autoaim'];
var auto=0;
var shieldstate=0;
var helperstate=0;

var win=0;
var dead = 0;
var pausesign;

var leaderboard;
var blastervolumn;
var effectsound=1;
var effectbar;
var bossbar;
var bgmvolumn;
var bgmsound=1;
var bgmbar;
var pausestate = false;

var playState = {

    preload: function() {
      

    },

    create: function() {

        game.stage.backgroundColor = '#0';
        
        //this.bg = game.add.sprite(0,0,'bg');
        //this.bg.scale.setTo(2, 1.5);
        this.bg = game.add.tileSprite(0, 0, 500, 781, "bg");

            
        this.player = game.add.sprite(game.width/2, 520, 'player');
        this.player.scale.setTo(0.5, 0.5);
        this.player.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        dead=0;
        speed=1;
        win=0;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.keyp = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.keyz = game.input.keyboard.addKey(Phaser.Keyboard.Z);  // Get key fire
        this.keyx = game.input.keyboard.addKey(Phaser.Keyboard.X);  // Get key fire
        
       
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(4, 'enemy');
        this.beforeBoss = game.time.events.loop(1500, this.addEnemy, this);

        this.enemies2 = game.add.group();
        this.enemies2.enableBody = true;
        this.enemies2.createMultiple(3, 'enemy2');

        this.items = game.add.group();
        this.items.enableBody = true;
        this.items.createMultiple(2, 'enemy2');

        this.pillars = game.add.group();
        this.pillars.enableBody = true;
        game.add.sprite(30, 0, 'pillar', 0, this.pillars);
        game.add.sprite(game.width-30, 0, 'pillar', 0, this.pillars);

        this.pillars.setAll('body.immovable', true);
      

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(15, 'bullet');

        this.ults = game.add.group();
        this.ults.enableBody = true;
        this.ults.createMultiple(3, 'suck_ball');
        
      
        
        ultnum=3;
        this.ultpicture = game.add.sprite(game.width - 80, 80, 'suck_ball');
        this.ultpicture.animations.add('fly', [ 0, 1, 2, 3, 4, 5], 20, true);
        this.ultpicture.animations.play('fly');
      //  this.ults.callAll('animations.add', 'animations', 'fly', [0,1,2,3], 10, true);
       //this.ults.callAll('play', null, 'fly');
        this.ults.callAll('animations.add', 'animations', 'fly3', [0,1,2,3], 16, true);
        this.ults.callAll('play', null, 'fly3');
 
        ultshow = game.add.text(game.world.width - 50, 80, ' x '+ ultnum, { font: '24px Arial', fill: '#0' });

        this.enemybullets = game.add.group();
        this.enemybullets.enableBody = true;
        this.enemybullets.createMultiple(4, 'fireball');

        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.enemies);   
        game.physics.arcade.enable(this.bullets);
        game.physics.arcade.enable(this.enemybullets);

        
        game.add.text(game.world.width - 80, 10, 'Lives ', { font: '24px Arial', fill: '#0' });
        this.heart = game.add.sprite(game.world.width - 80, 50, 'heart');
        this.heart.scale.setTo(0.8, 0.8);
        this.player.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        this.player.life=15;
        this.live = game.add.text(game.world.width - 50, 50, 'x '+ this.player.life, { font: '24px Arial', fill: '#0' });

        game.add.text( 10, 10, 'kills :', { font: '24px Arial', fill: '#0' });
        kills=0;
        this.killnum = game.add.text( 70, 10, kills, { font: '24px Arial', fill: '#0' });

        this.ults.callAll('animations.add', 'animations', 'fly3', [0,1,2,3], 16, true);
        

        this.blaster = game.add.audio('blaster');
        this.explode = game.add.audio('explode');
        this.hurt = game.add.audio('hurt');
        this.bgm_boss = game.add.audio('bgm_boss');
        this.winsound = game.add.audio('win');
        this.alarm = game.add.audio('alarm');
        

        this.bgm = game.add.audio('bgm');
        this.bgm.loopFull();    

       
        
        game.time.events.add(20000,function(){
           var event = game.time.events.loop(1350,this.addEnemy2,this);
           var harder = game.add.text(180, 200, 'Warning', { font: '45px Arial', fill: '#0' });
           speed=2;
           this.alarm.play();
           game.time.events.add(5000,function(){
            this.alarm.stop();
            harder.destroy();
            
            },this,harder);

           game.time.events.add(45000,function(){
               game.time.events.remove(event);
               
           },this,event);
        },this);
        game.time.events.add(65000,function(){
            speed=3;
            this.alarm.play();
            this.boss();
            this.bgm.stop();
            
        },this);
       
        this.ults.callAll('play', null, 'fly3');
    },

  
    update: function() {
        this.movePlayer();
      //  this.moveBackground();
        if(this.keyz.isDown){
            if(dead==1){
                this.bgm.stop();
                game.time.reset();
                game.state.start('play',true,false);         
            }
            else{
                 this.fire(this.player);
                
            }
           
        }
        this.attackCheck();
        this.damageCheck();
        this.lifeDisplay();
        this.killDisplay();
        if(this.keyp.isDown){
            this.playerPause();
        }
        game.physics.arcade.collide(this.enemies2, this.pillars);

        if(this.keyx.isDown){
            this.ultFire();
        }
        if(pausestate){
            game.input.onDown.add(this.volumnChange,this);
        }
        
        
        this.bg.tilePosition.y += speed;
        if(this.milos)
            game.physics.arcade.collide(this.milos, this.pillars);

            ultshow.destroy();
            ultshow = game.add.text(game.world.width - 50, 80, 'x '+ ultnum, { font: '24px Arial', fill: '#0'   });

        

        

        
        

    },

  
    movePlayer: function() {
        if(this.cursor.left.isDown){
            this.player.body.velocity.x=-300;
            this.player.scale.x = -0.5 ;
        }
       
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x=300; 
            this.player.scale.x = 0.5 ;  
        }
        else
            this.player.body.velocity.x=0;

         if(this.cursor.up.isDown){
            this.player.body.velocity.y=-300;
            if(dead==1){
                this.bgm.stop();

                game.state.start('menu',true,false);
            }
        }
        
        else if(this.cursor.down.isDown){
            this.player.body.velocity.y=300;   
        }
        else 
            this.player.body.velocity.y=0;

        if(shieldstate){

            this.shield.x=this.player.x;
            this.shield.y=this.player.y;

        }

        if(helperstate){
            this.helper.x=this.player.x +40;
            this.helper.y=this.player.y;

        }
        
       

    },
    moveBackground: function(){
        
        if(this.bg.y > game.height)
            this.bg.y = 0;
        else
            this.bg.y+=1;


    },
    addEnemy: function() {
        
        var enemy = this.enemies.getFirstDead();
      
        if (!enemy) { return;}
        
        enemy.anchor.setTo(0.5, 1);
        enemy.scale.setTo(0.5,0.5);
        
        enemy.reset(game.width/5*game.rnd.pick([1 ,2, 3, 4 ]), 0);
        
        enemy.body.velocity.y = 50;
    //    enemy.body.velocity.x = 50 * game.rnd.pick([-1, 1]);
        
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        enemy.life = 1;
        enemy.event = game.time.events.loop(1650, this.eneFire, this,enemy);

        game.time.events.add(7000,function(){
        game.time.events.remove(enemy.event);
        
        },this,enemy.event);

       
        },
    addEnemy2: function() {
        
           
            var enemy2 = this.enemies2.getFirstDead();
          
            if (!enemy2) { return;}
            
            enemy2.anchor.setTo(0.5, 1);
            enemy2.scale.setTo(0.5,0.5);
            
            enemy2.reset(game.width/10*game.rnd.pick([1 ,2, 3, 4, 5, 6, 7, 8, 9 ]), 0);
            
            enemy2.body.velocity.y = 100;
            enemy2.body.velocity.x = 50 * game.rnd.pick([-1, 1]);
            enemy2.body.bounce.x = 1;
            enemy2.checkWorldBounds = true;
            enemy2.outOfBoundsKill = true;
            enemy2.life=2;
            
            enemy2.event = game.time.events.loop(1750, this.eneFire2, this,enemy2);
    
            game.time.events.add(6000,function(){
            game.time.events.remove(enemy2.event);
        
            },this,enemy2.event);
    
    
            
         },

    playerDie: function(){

        leaderboard = game.add.sprite(game.width/2, game.height/2, 'leaderboard');
        
        leaderboard.alpha = 0.7;
        leaderboard.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        dead =1;

        if(win==1){


        }

         
        var newplayer = prompt("Please enter your name", "");
     //   localStorage.setItem("playerName", player);
     //   localStorage.getItem("playerName");  

     
     
        for(var i=0;i<6;i++){
            if(score[i]<kills){
                rank.splice(i,0,newplayer);
                score.splice(i,0,kills);
                this.star= game.add.sprite(70, 200+i*40, 'star');
                this.star.scale.setTo(0.2,0.2);
                
                break;

            }
        }
     


     for(var i=0; i<6;i++){
         game.add.text(100, 200+i*40, (i+1)+". "+rank[i], { font: '24px Arial', fill: '#fff' });
         game.add.text(400, 200+i*40, score[i], { font: '24px Arial', fill: '#fff' });
        
     }
    
     game.add.text(65, 440,"your name: "+newplayer, { font: '24px Arial', fill: '#fff' });
     game.add.text(400, 440, kills, { font: '24px Arial', fill: '#fff' });
        
     game.add.text(170, 480, "press z to play again", { font: '18px Arial', fill: '#fff' });
     game.add.text(120, 500, "press up arrow to back to menu", { font: '18px Arial', fill: '#fff' });
        this.player.kill();

        
    },

    fire: function(location){
               
            
        if(game.time.now > bulletTime){
            
            var bullet = this.bullets.getFirstDead();
                
            if (!bullet) { return;}
           
                
            bullet.anchor.setTo(0.5, 1);
            bullet.scale.setTo(0.1, 0.1);
            bullet.reset(location.x, location.y);
            if(auto){
                var enemy= this.enemies.getFirstAlive();
                if(enemy){
                    game.physics.arcade.moveToObject(bullet,enemy,1000);
                }
                else{
                    enemy= this.enemies2.getFirstAlive();
                    if(enemy){
                        game.physics.arcade.moveToObject(bullet,enemy,1000);
                    }
                    else{
                        bullet.body.velocity.y = -250;
                    }
                }
                
            }
            else{
                bullet.body.velocity.y = -250;
            }
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;
            this.blaster.play();
            bulletTime = game.time.now + 500;
        }

    },
    helpFire: function(location){
               
            
        
            
            var bullet = this.bullets.getFirstDead();
                
            if (!bullet) { return;}
           
                
            bullet.anchor.setTo(0.5, 1);
            bullet.scale.setTo(0.1, 0.1);
            bullet.reset(location.x, location.y);
            if(auto){
                var enemy= this.enemies.getFirstAlive();
                if(enemy){
                    game.physics.arcade.moveToObject(bullet,enemy,1000);
                }
                else{
                    enemy= this.enemies2.getFirstAlive();
                    if(enemy){
                        game.physics.arcade.moveToObject(bullet,enemy,1000);
                    }
                    else{
                        bullet.body.velocity.y = -250;
                    }
                }
                
            }
            else{
                bullet.body.velocity.y = -250;
            }
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;
            this.blaster.play();
    
       

    },
    ultFire: function(){
         var ult = this.ults.getFirstDead();
        if (!ult||!ultnum) { return;}  

    //    ult.animations.add('fly', [1,3 ], 8, true);
           
        if(game.time.now > ultTime && ultnum>0){
            
           
            
            if (!ult||!ultnum) { return;}  

            
            
            
            ultshow.destroy();
            ultshow = game.add.text(game.world.width - 50, 80, 'x '+ ultnum, { font: '24px Arial', fill: '#0'   });
            ultnum--;
           
            ult.anchor.setTo(0.5, 1);
            ult.reset(this.player.x, this.player.y);
           // ult.animations.play('fly');
            ult.body.velocity.y = -100;
            ult.body.velocity.x = this.player.body.velocity.x/4;
            ult.checkWorldBounds = true;
            ult.outOfBoundsKill = true;
            var event = game.time.events.loop(500, function(){

                this.enemies2.forEachAlive(function(enemy){

                    game.physics.arcade.moveToObject(enemy,ult,200);
              
                }, this, ult)

            }, this,this.enemies2,ult);
            var event2 = game.time.events.loop(500, function(){

                this.enemies.forEachAlive(function(enemy){

                    game.physics.arcade.moveToObject(enemy,ult,200);
              
                }, this, ult)

            }, this,this.enemies,ult);
            var event3 = game.time.events.loop(500, function(){

                this.enemybullets.forEachAlive(function(enemy){

                    game.physics.arcade.moveToObject(enemy,ult,200);
              
                }, this, ult)

            }, this,this.enemybullets,ult);


            game.time.events.add(2500,function(){
                    game.time.events.remove(event);
                   

                },this,event);
            game.time.events.add(2500,function(){
                    game.time.events.remove(event2);
                   

                },this,event2);
            game.time.events.add(2500,function(){
                    game.time.events.remove(event3);
                   

                },this,event3);

           // this.blaster.play();
            ultTime = game.time.now + 500;

        }

    },

    eneFire: function(enemy){


        var enemybullet = this.enemybullets.getFirstDead();
        if (!enemybullet) { return;}
    
        enemybullet.anchor.setTo(0.5, 1);
        enemybullet.scale.setTo(0.1, 0.1);
        enemybullet.reset(enemy.x, enemy.y);
        enemybullet.body.velocity.y = 300;
        enemybullet.checkWorldBounds = true;
        enemybullet.outOfBoundsKill = true;
    
    
      
    

       

   
    },
    eneFire2: function(enemy){
               
        
         
                var enemybullet = this.enemybullets.getFirstDead();
                if (!enemybullet) { return;}

                enemybullet.anchor.setTo(0.5, 1);
                enemybullet.scale.setTo(0.1, 0.1);
                enemybullet.reset(enemy.x, enemy.y);
                game.physics.arcade.moveToObject(enemybullet,this.player,500);
                enemybullet.checkWorldBounds = true;
                enemybullet.outOfBoundsKill = true;

            
         

   
    },

    attackCheck: function(){

        game.physics.arcade.overlap(this.bullets, this.enemies, this.enemyWithBullet, null, this);
        game.physics.arcade.overlap(this.bullets, this.enemies2, this.enemy2WithBullet, null, this);
        if(this.milos){
            game.physics.arcade.overlap(this.bullets,this.milos, this.bossWithBullet, null, this);
        }
    },

    damageCheck: function(){
        game.physics.arcade.overlap(this.player,this.enemies, this.enemyWithPlayer, null, this);
        game.physics.arcade.overlap(this.player,this.enemies2, this.enemyWithPlayer, null, this);
        game.physics.arcade.overlap(this.player,this.enemybullets, this.enemyWithPlayer, null, this);
        game.physics.arcade.overlap(this.player,this.items, this.itemWithPlayer, null, this);
        if(this.milos){
            game.physics.arcade.overlap(this.player,this.milos, this.bossWithPlayer, null, this);
        }
    },

    enemyWithBullet: function(bullet, enemy) {
        
        this.emitter = game.add.emitter(enemy.x, enemy.y, 15);
        this.emitter.makeParticles('boom');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.start(true, 800, null, 15);
        game.time.events.remove(enemy.event);
        game.time.events.remove(bullet.event);
        bullet.kill();
        enemy.life--;
        if(!enemy.life){
           enemy.kill(); 
           kills++;
           var chance = game.rnd.pick([1,2,3,4,5,6,7,8,9,10]);
       // var chance =10;
        var myitem = game.rnd.pick(itemzone);

        if(chance>=9){
            this.genItem(myitem,enemy);
        }
        }
        
        this.explode.play();
        

        

        

    
    },
    enemy2WithBullet: function(bullet, enemy) {
        
        this.emitter = game.add.emitter(enemy.x, enemy.y, 15);
        this.emitter.makeParticles('boom');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.start(true, 800, null, 15);
        game.time.events.remove(enemy.event);
        game.time.events.remove(bullet.event);
        bullet.kill();
        enemy.life--;
        enemy.loadTexture('enemy', 0);
        if(!enemy.life){
            enemy.loadTexture('enemy2', 0);
            enemy.kill();
            kills++;
            var chance = game.rnd.pick([1,2,3,4,5,6,7,8,9,10]);
            var myitem = game.rnd.pick(itemzone);

            if(chance>=9){
                 this.genItem(myitem,enemy);
            }
        
        }
        
        this.explode.play();
        
        
    
    },
    enemyWithPlayer: function(player, enemy) {
        
        
        game.time.events.remove(enemy.event);
        this.hurt.play();
       
        enemy.kill();
        if(!shieldstate){
            player.life--;
            this.saveTem();
        }
       
            
        
        
        if(player.life==0){
            this.bgm.stop();
            this.playerDie();
        }
        
    
    },
   itemWithPlayer: function(player, item) {
        
        
        
        item.kill();
        if(item.label=='autoaim'){
            auto=1;
            
           game.time.events.add(10000,function(){
               auto=0;
           },this);
           
        }
        else if(item.label=='recover'){
            this.player.life+=1;
        }
        else if(item.label=='helper'){

            if(!helperstate){
                helperstate=1;
            this.helper = game.add.sprite(this.player.x+60, this.player.y, 'helper');
            this.helper.scale.setTo(0.6, 0.6);
            this.helper.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
            var event = game.time.events.loop(750,function(){
                
                this.helpFire(this.helper);
            },this,this.helper,this.helpFire);

            game.time.events.add(16900,function(){
                helperstate=0;
                this.helper.kill();
                game.time.events.remove(event);

            },this,this.helper,event);
            }

            


           
        }
        else if(item.label=='shield'){
            if(!shieldstate){
                shieldstate=1;
            this.shield = game.add.sprite(this.player.x, this.player.y, 'shield');
            this.shield.alpha=0.7;
            this.shield.scale.setTo(0.8, 0.8);
            this.shield.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.

            game.time.events.add(10000,function(){
                shieldstate=0;
                this.shield.kill();

            },this,this.shield);
            }
 
        }
      
        
    
    },
    lifeDisplay: function(){
        this.live.destroy();
        this.live = game.add.text(game.world.width - 50, 50, 'x '+ this.player.life, { font: '24px Arial', fill: '#0' });
  

    },
    killDisplay: function(){
        this.killnum.destroy();
        this.killnum = game.add.text( 70, 10, kills, { font: '24px Arial', fill: '#0' });

    },
    playerPause: function(){

        
        leaderboard = game.add.sprite(game.width/2, game.height/2, 'leaderboard');  
        leaderboard.alpha = 0.7;
        leaderboard.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        pausesign = game.add.text(150, 200, 'Setting', { font: '68px Arial', fill: '#fff' });
        blastervolumn = game.add.text(120, 360, 'effectvolumn:    [   +   ]   [   -   ]   ', { font: '18px Arial', fill: '#fff' });
        effectbar = game.add.sprite(360, 360, 'progressBar');
        effectbar.scale.x =effectsound;
        effectbar.scale.y= 0.5;
        
        bgmvolumn = game.add.text(120, 400, 'bgm volumn:     [   +   ]   [   -   ]', { font: '18px Arial', fill: '#fff' });
        bgmbar = game.add.sprite(360, 400, 'progressBar');
        bgmbar.scale.x =bgmsound;
        bgmbar.scale.y= 0.5;
        game.paused = true;

        pausestate=true;

        this.keyp.onDown.add(function(){
        game.paused = false;
        pausestate = false;
        pausesign.destroy();
        leaderboard.destroy();
        blastervolumn.destroy();
        bgmvolumn.destroy();
        effectbar.destroy();
        bgmbar.destroy();
    }, this);
    },
    volumnChange: function(pointer){
        if(pointer.x>312 && pointer.x <352 && pointer.y<378 && pointer.y> 360){
            this.blaster.volume-= 0.1;
            this.explode.volume-= 0.1;
            if(effectsound>0){
                effectbar.destroy();
                effectsound-=0.1;
                effectbar = game.add.sprite(360, 360, 'progressBar');
                effectbar.scale.x =effectsound;
                effectbar.scale.y= 0.5;
                
            }
        }
        else if(pointer.x<292 && pointer.x>248 && pointer.y<378 && pointer.y> 360){
            this.blaster.volume+= 0.1;
            this.explode.volume+= 0.1;
            if(effectsound<1){
                effectbar.destroy();
                effectsound+=0.1;
                effectbar = game.add.sprite(360, 360, 'progressBar');
                effectbar.scale.x =effectsound;
                effectbar.scale.y= 0.5;
                
                
            }
        }
        else if(pointer.x>312 && pointer.x <352 && pointer.y<418 && pointer.y> 400){
            this.bgm.volume-= 0.1;
            if(bgmsound>0){
                bgmbar.destroy();
                bgmsound-=0.1;
                bgmbar = game.add.sprite(360, 400, 'progressBar');
                bgmbar.scale.x =bgmsound;
                bgmbar.scale.y= 0.5;
               
                
                
            }
        }
        else if(pointer.x>312 && pointer.x <352 && pointer.y<418 && pointer.y> 400){
            this.bgm.volume+= 0.1;
            if(bgmsound<1){
                bgmbar.destroy();
                bgmsound+=0.1;
                bgmbar = game.add.sprite(360, 400, 'progressBar');
                bgmbar.scale.x =bgmsound;
                bgmbar.scale.y= 0.5;
                
               
            }
        }
        
        
    },
    boss: function(){
        
        
        game.time.events.remove(this.beforeBoss);
        var bosscome = game.add.text(120, 200, 'BOSS coming', { font: '45px Arial', fill: '#0' });
        game.time.events.add(3000,function(){
            bosscome.destroy();
        },this.bosscome);
        this.milos = game.add.sprite(game.width/2,0, 'boss');
    //    this.milos.scale.setTo(0.5, 0.5);
        this.milos.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        game.physics.arcade.enable(this.milos);
        game.time.events.loop(200,this.bossMove,this);
        this.milos.body.bounce.x = 1;
        this.milos.ready=0;
        this.milos.hug=0;
        this.milos.life=60;
        bossbar = game.add.sprite(60, 36, 'bossbar');
        
        


    },
    bossMove:function(){

        bossbar.destroy();        
        bossbar = game.add.sprite(60, 36, 'bossbar');
        bossbar.scale.x =this.milos.life/60;
        bossbar.scale.y= 0.5;

        if(!this.milos.ready){
            if(this.milos.y<100){
            this.milos.body.velocity.y=40;
            }
            else{
                this.milos.body.velocity.x=50;
                this.milos.body.velocity.y=0;
                this.milos.ready=1;
                this.alarm.stop();
                this.bgm_boss.loopFull();
            }
        }
        else{
            if(this.milos.hug!=5){
                if(this.milos.y>150){
                    this.milos.body.velocity.y=-200;
                    

                }
                else{
                    this.milos.body.velocity.y=0;
                    this.bossFire();
                    
                    if((this.player.x-this.milos.x)>0){
                   
                    if(this.milos.pass!=-1){
                         this.milos.hug++;
                    }
                    this.milos.pass=-1;
                    
       
                }
                else if((this.player.x-this.milos.x)<0){
                    if(this.milos.pass!=1){
                        this.milos.hug++;
                   }
                    this.milos.pass=1;
                }
                }

                if(this.milos.hug==4){
                    this.milos.loadTexture('milos_wait');
                    game.time.events.add(500,function(){
                        this.milos.loadTexture('boss');
                    },this,this.milos);
                }
            
                

            }
            else{
                
                if(this.milos.y<600){
                    this.milos.body.velocity.y=350;
                    this.milos.body.velocity.x=0;
                }
                else{
                    this.milos.body.velocity.y=-200;
                    this.milos.hug=0;
                    game.time.events.add(5000,function(){
                        this.milos.body.velocity.x=-50;

                    },this,this.milos)
                }
                

            }



        }

        
    },
    bossFire:function(){
        var enemybullet = this.enemybullets.getFirstDead();
         if (!enemybullet) { return;}

        enemybullet.anchor.setTo(0.5, 1);
        enemybullet.scale.setTo(0.5, 0.5);
        enemybullet.reset(this.milos.x, this.milos.y);
        game.physics.arcade.moveToObject(enemybullet,this.player,300);
        enemybullet.checkWorldBounds = true;
        enemybullet.outOfBoundsKill = true;

        var enemybullet2 = this.enemybullets.getFirstDead();
        if (!enemybullet2) { return;}
        enemybullet2.anchor.setTo(0.5, 1);
        enemybullet2.scale.setTo(0.5, 0.5);
        enemybullet2.reset(this.milos.x, this.milos.y);
        enemybullet2.body.velocity.y=100;
        enemybullet2.checkWorldBounds = true;
        enemybullet2.outOfBoundsKill = true;
        
        


    },
    bossWithBullet:function(milos,bullet){
        
        
        bullet.kill();
        this.milos.life--;
        this.milos.loadTexture('milos_hurt', 0);
        game.time.events.add(500,function(){
            this.milos.loadTexture('boss', 0);
        },this,this.milos)
        var chance = game.rnd.pick([1,2,3,4,5,6,7,8,9,10]);
        var myitem = game.rnd.pick(itemzone);

            if(chance>=9){
                 this.genItem(myitem,this.milos);
            }


        if(!this.milos.life){
            this.emitter = game.add.emitter(this.milos.x, this.milos.y, 50);
        this.emitter.makeParticles('boom');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 10000);
        this.emitter.start(true, 10000, null, 50);
            this.bgm_boss.stop();
            this.milos.kill();
            win=1;
            kills+=30;
            this.explode.play();
            this.winsound.play();
            game.time.events.add(6000,function(){
                this.playerDie();

            },this,this.playerDie);
            
        }
        
        


    },
    bossWithPlayer:function(){
        this.hurt.play();
       
        
        if(!shieldstate){
            this.player.life--;
            this.saveTem();
        }
       

        
        if(this.player.life==0){
            this.playerDie();
        }


    },

    genItem: function(myitem,enemy){

        var item = this.items.getFirstDead();
        if (!item) { return;}
        item.anchor.setTo(0.5, 1);
        item.scale.setTo(0.5,0.5);


        if(myitem=='autoaim'){
            item.loadTexture('autoaim', 0);
            item.label= 'autoaim';

        }
        else if(myitem=='recover'){
            
            item.loadTexture('heart', 0);
            item.label= 'recover';

        }
        else if(myitem=='helper'){
            item.loadTexture('helper', 0);
            item.label= 'helper';
            
        }
        else if(myitem=='shield'){
            item.loadTexture('shield', 0);
            item.label= 'shield';

            
            
        }
        item.reset(enemy.x, enemy.y);       
        item.body.velocity.y = 45;
        item.checkWorldBounds = true;
        item.outOfBoundsKill = true;

    },
    saveTem:function(){
        shieldstate=1;
        this.shield = game.add.sprite(this.player.x, this.player.y, 'shield');
        this.shield.alpha=0.7;
        this.shield.scale.setTo(0.8, 0.8);
        this.shield.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.

        game.time.events.add(3000,function(){
            shieldstate=0;
            this.shield.kill();

        },this,this.shield);
    }
};